#!/usr/bin/env python3

__copyright__ = 'All rights reserved'
__author__ = 'Brandon Staab'
__email__ = 'bls114@zips.uakron.edu'

import matplotlib.pyplot as plt
import pandas as pd

from functools import reduce

# pip3 install --user pandas==1.0.0
assert pd.__version__ == '1.0.0'


def read_table(filename, names):
	return pd.read_table(filename, sep='::', header=None, names=names, engine='python')


def read_tables(data_dir):
	movies_names = ['movie_id', 'title', 'genres']
	ratings_names = ['user_id', 'movie_id', 'rating', 'timestamp']
	users_names = ['user_id', 'gender', 'age', 'occupation', 'zip']

	movies = read_table(data_dir + 'movies.dat', movies_names)
	ratings = read_table(data_dir + 'ratings.dat', ratings_names)
	users = read_table(data_dir + 'users.dat', users_names)

	return users, ratings, movies


def merge_tables(tables, how):
	assert len(tables) >= 2
	return reduce(lambda left, right: pd.merge(left, right, how=how), tables)


def rows_containing_na(df):
	assert type(df) is pd.DataFrame
	return df.isna().any(axis=1)


def total_unique(df, cols):
	assert type(df) is pd.DataFrame
	assert type(cols) is list
	return len(df[cols].drop_duplicates())


def plot_ratings_per_user(df):
	ratings_per_user = df.groupby('user_id').size().sort_values(ascending=False, ignore_index=True)

	plot = ratings_per_user.plot.area()
	plot.set_title('Ratings per User')
	plot.set_xlabel('User Rank by Number of Ratings')
	plot.set_ylabel('Ratings')
	plt.show()


def get_movie_years(df):
	return df['title'].drop_duplicates().apply(lambda x: int(x[-5:-1]))


def plot_movies_per_year(df):
	movies_per_year = get_movie_years(df).value_counts().sort_index()

	plot = movies_per_year.plot.area(rot=0)
	plot.set_title('Movies per Year')
	plot.set_xlabel('Year')
	plot.set_ylabel('Movies')
	plt.show()


def plot_movies_per_genre(df):
	movies = df[['movie_id', 'genres']].drop_duplicates()
	movies_per_genre = movies['genres'].value_counts().sort_values(ascending=False)

	plot = movies_per_genre.plot.bar()
	plot.set_title('Movies per Genre')
	plot.set_ylabel('Movies')
	plt.show()


def avg_rating_per(feature, df):
	return df[[feature, 'rating']].groupby(feature).mean().sort_values(by='rating', ascending=False)


def plot_avg_rating_per_movie(df):
	plot = avg_rating_per('title', df).plot.bar()
	plot.set_title('Average Rating per Movie')
	plot.set_ylabel('Average Rating')
	plt.show()


def plot_avg_rating_per_genre(df):
	plot = avg_rating_per('genres', df).plot.bar()
	plot.set_title('Average Rating per Genre')
	plot.set_ylabel('Average Rating')
	plt.show()

def best_ratings_by_user(df):
	return df.groupby('user_id').apply(lambda grp: grp.nlargest(10, 'rating'))

#### Questions #################################################################

def question_1(data_dir):
	print('Question #1:')

	df = merge_tables(read_tables(data_dir), 'outer')
	nans = rows_containing_na(df)
	print(f'Removing {nans.sum()} rows that contained NaN.')

	return df[nans == False]


def question_2(df):
	print('\nQuestion #2:')
	print('Total number of:')
	print('  - Movies:  ', total_unique(df, ['movie_id']))
	print('  - Genres:  ', total_unique(df, ['genres']))
	print('  - Users:   ', total_unique(df, ['user_id']))
	print('  - Ratings: ', total_unique(df, ['movie_id', 'user_id', 'timestamp']))


def question_3(df):
	print('\nQuestion #3:')
	plot_ratings_per_user(df)
	plot_movies_per_year(df)
	plot_movies_per_genre(df)
	plot_avg_rating_per_movie(df)
	plot_avg_rating_per_genre(df)


def question_4(df):
	print('\nQuestion #4:')

	gtoe_median = df['rating'] >= df['rating'].median()
	print(f'Ratings greater than or equal to median: {gtoe_median.sum()}')

	print('Top ten movies by user showing title and genres:')
	print(best_ratings_by_user(df)[['title', 'genres']])

	print('Average rating of genres for each user:')
	print(df.groupby(['user_id','genres'])['rating'].mean())



#### Main ######################################################################

def main():
	pd.options.display.max_rows = 10
	data_dir = '../datasets/movielens/'

	df = question_1(data_dir)
	question_2(df)
	question_3(df)
	question_4(df)

if __name__ == '__main__':
	main()

