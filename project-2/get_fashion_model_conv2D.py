from tensorflow.keras.layers import (
	Conv2D,
	Dense,
	Dropout,
	Flatten,
	MaxPooling2D,
)

from tensorflow.keras.models import Sequential


def get_fashion_model_conv2D(input_shape, num_classes):
	model = Sequential([
		Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape),
		Conv2D(32, (3, 3), activation='relu'),
		MaxPooling2D(pool_size=(2, 2)),
		Dropout(0.25),

		Conv2D(64, (3, 3), padding='same', activation='relu'),
		Conv2D(64, (3, 3), activation='relu'),
		MaxPooling2D(pool_size=(2, 2)),
		Dropout(0.25),

		Flatten(),
		Dense(512, activation='relu'),
		Dropout(0.5),

		Dense(num_classes, activation='softmax'),
	])


	model.compile(
		optimizer='nadam',
		loss='categorical_crossentropy',
		metrics=['accuracy']
	)

	return model
