from tensorflow.keras.layers import (
	BatchNormalization,
	Conv2D,
	Dense,
	Dropout,
	Flatten,
	MaxPooling2D,
)

from tensorflow.keras.models import Sequential
from tensorflow.keras.regularizers import l2


def get_cifar10_model_conv2D(input_shape, num_classes):
	model = Sequential([
		Conv2D(64, (3, 3), padding='same', activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4), input_shape=input_shape),
		Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4)),
		MaxPooling2D(pool_size=(2, 2)),
		BatchNormalization(),
		Dropout(0.25),

		Conv2D(64, (3, 3), padding='same', activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4)),
		Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4)),
		MaxPooling2D(pool_size=(2, 2)),
		BatchNormalization(),
		Dropout(0.25),

		Conv2D(64, (5, 5), padding='same', activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4)),
		Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', kernel_regularizer=l2(1e-4)),
		MaxPooling2D(pool_size=(2, 2)),
		BatchNormalization(),
		Dropout(0.25),

		Flatten(),
		Dense(256, activation='relu'),
		BatchNormalization(),
		Dropout(0.5),

		Flatten(),
		Dense(128, activation='relu'),
		BatchNormalization(),
		Dropout(0.5),

		Dense(num_classes, activation='softmax'),
	])


	model.compile(
		optimizer='nadam',
		loss='categorical_crossentropy',
		metrics=['accuracy']
	)

	return model
