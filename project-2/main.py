#!/usr/bin/env python3.7

import os
import warnings

from datetime import datetime
from pathlib import Path

# disable some tesorflow warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from get_fashion_dataset import get_fashion_dataset
from get_fashion_model_dense import get_fashion_model_dense
from get_fashion_model_conv2D import get_fashion_model_conv2D

from get_cifar10_dataset import get_cifar10_dataset
from get_cifar10_model_conv2D import get_cifar10_model_conv2D

from tensorflow.keras import callbacks
from sklearn.metrics import (
	confusion_matrix,
	ConfusionMatrixDisplay,
)

import matplotlib.pyplot as plt
import numpy as np


def prepare_model_dir(name):
	date = datetime.today().strftime('%Y-%m-%d')
	time = datetime.today().strftime('%H-%M-%S')
	Path(f'./models/{name}/{date}').mkdir(parents=True, exist_ok=True)

	return (date, time)


def get_callbacks(name, date, time):
	return [
		callbacks.EarlyStopping(patience=8, verbose=1, restore_best_weights=True),
		callbacks.ReduceLROnPlateau(patience=4, verbose=1),
		callbacks.CSVLogger(f'./models/{name}/{date}/{time}_training.csv'),
		callbacks.TensorBoard(f'./models/{name}/{date}'),
	]


def plot_model_history(history):
	plt.suptitle('Loss')
	plt.plot(history.history['loss'], label='train')
	plt.plot(history.history['val_loss'], label='test')
	plt.legend()
	plt.show()

	plt.suptitle('Accuracy')
	plt.plot(history.history['acc'], label='train')
	plt.plot(history.history['val_acc'], label='test')
	plt.legend()
	plt.show()


def	plot_confusion_matrix(model, name, validation_generator, batch_size, classes, num_of_test_samples):
	matrix = np.zeros((len(classes), len(classes)))

	for batch_idx in range(int(num_of_test_samples / batch_size)):
		(x_test, y_true) = next(validation_generator)
		y_pred = model.predict(x_test, batch_size=batch_size)
		matrix += confusion_matrix(
			y_true=np.argmax(y_true, axis=1),
			y_pred=np.argmax(y_pred, axis=1),
			labels=[x for x in range(len(classes))]
		)

	matrix /= matrix.sum(axis=1)
	ConfusionMatrixDisplay(matrix, display_labels=classes).plot()
	plt.suptitle(f'{name} Confusion Matrix')
	plt.show()


def validate_model(get_model, get_dataset, name):
	(train_generator, validation_generator, batch_size, classes) = get_dataset()

	input_shape = train_generator.x.shape[1:]
	num_classes = train_generator.y.shape[1]
	num_of_train_samples = train_generator.x.shape[0]
	num_of_test_samples  = train_generator.y.shape[0]

	(date, time) = prepare_model_dir(name)

	model = get_model(input_shape, num_classes)
	model.summary()

	history = model.fit_generator(
		train_generator,
		validation_data=validation_generator,
		callbacks=get_callbacks(name, date, time),

		epochs=int(1e6),
		steps_per_epoch=num_of_train_samples // batch_size,
		validation_steps=num_of_test_samples // batch_size,
	)

	model.save(f'./models/{name}/{date}/{time}.h5')
	plot_model_history(history)
	plot_confusion_matrix(model, name, validation_generator, batch_size, classes, num_of_test_samples)


def main():
	models = [
		(get_fashion_model_dense, get_fashion_dataset, 'dense_fashion'),
		(get_fashion_model_conv2D, get_fashion_dataset, 'conv2d_fashion'),
		(get_cifar10_model_conv2D, get_cifar10_dataset, 'conv2d_cifar10'),

	]

	for get_model, get_dataset, name in models:
		 if 'y' == input(f'Validate {name} model? [y/N]').lower():
			 validate_model(get_model, get_dataset, name)


if __name__ == '__main__':
	main()

