from tensorflow.keras import utils
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import numpy as np


def get_cifar10_dataset():
	(x_train, y_train), (x_test, y_test) = cifar10.load_data()

	classes = [
		'Airplane',
		'Automobile',
		'Bird',
		'Cat',
		'Deer',
		'Dog',
		'Frog',
		'Horse',
		'Ship',
		'Truck',
	]

	train_datagen = ImageDataGenerator(
		featurewise_center            = True,
		featurewise_std_normalization = True,

		horizontal_flip    = True,
		fill_mode          = 'nearest',
		rotation_range     = 45.0,
		width_shift_range  = 0.2,
		height_shift_range = 0.2,
		shear_range        = 0.2,
		zoom_range         = 0.2,
	)

	test_datagen = ImageDataGenerator(
		featurewise_center            = True,
		featurewise_std_normalization = True,
	)

	# reshape so channel is last feature
	x_train = x_train.reshape(x_train.shape)
	x_test = x_test.reshape(x_test.shape)

	# convert class vectors to binary class matrices
	num_classes = len(np.unique(y_train))
	y_train = utils.to_categorical(y_train, num_classes)
	y_test = utils.to_categorical(y_test, num_classes)

	# fit data generators
	train_datagen.fit(x_train)
	test_datagen.fit(x_test)

	# flow train and validation generators
	batch_size = 32
	train_generator = train_datagen.flow(x_train, y_train, batch_size=batch_size)
	validation_generator = test_datagen.flow(x_test, y_test, batch_size=batch_size)

	return (train_generator, validation_generator, batch_size, classes)
