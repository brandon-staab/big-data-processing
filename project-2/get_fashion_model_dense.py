from tensorflow.keras.layers import (
	Dense,
	Dropout,
	Flatten,
)

from tensorflow.keras.models import Sequential


def get_fashion_model_dense(input_shape, num_classes):
	model = Sequential([
		Dense(128, activation='relu', input_shape=input_shape),
		Flatten(),
		Dropout(0.1),

		Dense(num_classes, activation='softmax'),
	])

	model.compile(
		optimizer='nadam',
		loss='categorical_crossentropy',
		metrics=['accuracy']
	)

	return model

